#!/usr/bin/python3

# Copyright (C) 2017 Infineon Technologies & pmdtechnologies ag
#
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.

"""This sample shows how to shows how to capture image data.

It uses Python's numpy and matplotlib to process and display the data.
"""

import argparse
import roypy
import time
import queue

from sample_camera_info import print_camera_info
from roypy_sample_utils import CameraOpener, add_camera_opener_options
from roypy_platform_utils import PlatformHelper

import numpy as np
import matplotlib.pyplot as plt

import pptk
import plotly.express as px
import pandas as pd

class MyListener(roypy.IDepthDataListener):
    def __init__(self, q):
        super(MyListener, self).__init__()
        self.queue = q

    def onNewData(self, data):
        zvalues = []
        xvalues = []
        yvalues = []
        grayvalues = []

        for i in range(data.getNumPoints()):
            zvalues.append(data.getZ(i))
            xvalues.append(data.getX(i))
            yvalues.append(data.getY(i))
            grayvalues.append(data.getGrayValue(i))

        zarray = np.asarray(zvalues)
        xarray = np.asarray(xvalues)
        yarray = np.asarray(yvalues)
        garray = np.asarray(grayvalues)

        p = np.concatenate([xarray, yarray, zarray, garray])

        self.queue.put(p)

    def paint_plotly(self, pointcloud):
        print(pointcloud)
        x, y, z, g = np.hsplit(pointcloud, 4)

        x_r = x.reshape(38304, 1)
        y_r = y.reshape(38304, 1)
        z_r = z.reshape(38304, 1)
        g_r = g.reshape(38304, 1)

        xyz_r = np.concatenate([x_r, y_r, z_r, g_r], axis=1)

        df = pd.DataFrame(data=xyz_r, columns=["x", "y", "z", "grauwert"])
        print(df)
        fig = px.scatter_3d(df, x="x", y="y", z="z", color="grauwert")
        fig.show()

def main ():
    platformhelper = PlatformHelper()
    parser = argparse.ArgumentParser (usage = __doc__)
    add_camera_opener_options (parser)
    parser.add_argument ("--seconds", type=int, default=15, help="duration to capture data")
    options = parser.parse_args()
    opener = CameraOpener (options)
    cam = opener.open_camera ()

    print_camera_info (cam)
    print("isConnected", cam.isConnected())
    print("getFrameRate", cam.getFrameRate())

    # we will use this queue to synchronize the callback with the main
    # thread, as drawing should happen in the main thread
    q = queue.Queue()
    l = MyListener(q)
    cam.registerDataListener(l)
    cam.startCapture()
    # create a loop that will run for a time (default 15 seconds)
    process_event_queue (q, l, options.seconds)
    cam.stopCapture()

def process_event_queue (q, painter, seconds):
    # create a loop that will run for the given amount of time
    t_end = time.time() + seconds
    while time.time() < t_end:
        try:
            # try to retrieve an item from the queue.
            # this will block until an item can be retrieved
            # or the timeout of 1 second is hit
            item = q.get(True, 1)
        except queue.Empty:
            # this will be thrown when the timeout is hit
            break
        else:
            painter.paint_plotly(item)

if (__name__ == "__main__"):
    main()
