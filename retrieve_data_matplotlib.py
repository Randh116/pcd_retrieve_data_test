#!/usr/bin/python3

# Copyright (C) 2017 Infineon Technologies & pmdtechnologies ag
#
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.

"""This sample shows how to shows how to capture image data.

It uses Python's numpy and matplotlib to process and display the data.
"""

import argparse
import roypy
import time
import queue

from sample_camera_info import print_camera_info
from roypy_sample_utils import CameraOpener, add_camera_opener_options
from roypy_platform_utils import PlatformHelper

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


class MyListener(roypy.IDepthDataListener):
    def __init__(self, q):
        super(MyListener, self).__init__()
        self.queue = q

    def onNewData(self, data):
        print(dir(data))

        zvalues = []
        xvalues = []
        yvalues = []
        grayvalues = []
        noisevalues = []

        for i in range(data.getNumPoints()):
             zvalues.append(data.getZ(i))
             xvalues.append(data.getX(i))
             yvalues.append(data.getY(i))
             grayvalues.append(data.getGrayValue(i))
             noisevalues.append(data.getNoise(i))

        zarray = np.asarray(zvalues)
        xarray = np.asarray(xvalues)
        yarray = np.asarray(yvalues)
        garray = np.asarray(grayvalues)
        narray = np.asarray(noisevalues)

        x = xarray.reshape(-1, data.width)
        y = yarray.reshape(-1, data.width)
        z = zarray.reshape(-1, data.width)
        g = garray.reshape(-1, data.width)
        n = narray.reshape(-1, data.width)

        p = np.concatenate((x, y, z, g, n), axis=0)

        self.queue.put(p)

        #CODE WITH BUGS - DO NOT USE!
        #pcl_array = np.asarray(point_cloud)
        #p = pcl_array.reshape(-1, data.width)
        #map(self.queue.put, [xarray, yarray, zarray])
        #for i in items:
        #    self.queue.put(i)


    def paint_xyz (self, data):
        """Called in the main thread, with data containing one of the items that was added to the
        queue in onNewData.
        """

        #x = data[::3, :]
        #y = data[1::3, :]
        #z = data[2::3, :]


        x, y, z, g, n = np.vsplit(data, 5)
        #print([x, y, z])
        add = x+y+z

        titles = ['x val', 'y val', 'z val', 'add']
        images = [x, y, z, add]
        size = 4

        for i in range(size):
            plt.subplot(1, size, i+1)
            plt.imshow(images[i])
            plt.title(titles[i])

        plt.show(block = False)
        plt.draw()
        # this pause is needed to ensure the drawing for
        # some backends
        plt.pause(0.001)


    def paint_noise (self, data):
        """Called in the main thread, with data containing one of the items that was added to the
        queue in onNewData.
        """
        # create a figure and show the raw data

        #x = data[::3, :]
        #y = data[1::3, :]
        #z = data[2::3, :]

        x, y, z, g, n = np.vsplit(data, 5)
        #print([x, y, z])
        add = g+n

        titles = ['z val', 'gray val', 'noise', 'gray+noise']
        images = [z, g, n, add]
        size = 4

        for i in range(size):
            plt.subplot(1, size, i+1)
            plt.imshow(images[i])
            plt.title(titles[i])

        plt.show(block = False)
        plt.draw()
        # this pause is needed to ensure the drawing for
        # some backends
        plt.pause(0.001)


    def paint_3d(self, data):
        x, y, z, g, n = np.vsplit(data, 5)
        fig = plt.figure()
        ax = Axes3D(fig)

        ax.scatter(x, y, z, marker='^')
        plt.show(block = False)
        plt.draw()
        plt.pause(0.01)



#I dont use these functions, but I left it here for future usage
def show_images(images, cols=1, titles=None):
    """Display a list of images in a single figure with matplotlib.

    Parameters
    ---------
    images: List of np.arrays compatible with plt.imshow.

    cols (Default = 1): Number of columns in figure (number of rows is
                        set to np.ceil(n_images/float(cols))).

    titles: List of titles corresponding to each image. Must have
            the same length as titles.
    """
    assert ((titles is None) or (len(images) == len(titles)))
    n_images = len(images)
    if titles is None: titles = ['Image (%d)' % i for i in range(1, n_images + 1)]
    fig = plt.figure()
    for n, (image, title) in enumerate(zip(images, titles)):
        a = fig.add_subplot(cols, np.ceil(n_images / float(cols)), n + 1)
        if image.ndim == 2:
            plt.gray()
        plt.imshow(image)
        a.set_title(title)
    #fig.set_size_inches(np.array(fig.get_size_inches()) * n_images)
    plt.show()


def plot_figures(figures):
    fig = plt.figure(1)
    k = 1
    for title in figures:
        ax = fig.add_subplot(len(figures),1,k)
        ax.imshow(figures[title])
        ax.gray()
        ax.title(title)
        ax.axis('off')
        k += 1


def main ():
    platformhelper = PlatformHelper()
    parser = argparse.ArgumentParser (usage = __doc__)
    add_camera_opener_options (parser)
    parser.add_argument ("--seconds", type=int, default=30, help="duration to capture data")
    options = parser.parse_args()
    opener = CameraOpener (options)
    cam = opener.open_camera ()

    print_camera_info (cam)
    print("isConnected", cam.isConnected())
    print("getFrameRate", cam.getFrameRate())

    # we will use this queue to synchronize the callback with the main
    # thread, as drawing should happen in the main thread
    q = queue.Queue()
    l = MyListener(q)
    cam.registerDataListener(l)
    cam.startCapture()
    # create a loop that will run for a time (default 15 seconds)
    process_event_queue (q, l, options.seconds)
    cam.stopCapture()


def process_event_queue (q, painter, seconds):
    # create a loop that will run for the given amount of time
    t_end = time.time() + seconds
    while time.time() < t_end:
        try:
            # try to retrieve an item from the queue.
            # this will block until an item can be retrieved
            # or the timeout of 1 second is hit
            item = q.get(True, 1)
        except queue.Empty:
            # this will be thrown when the timeout is hit
            break
        else:
            #painter.paint_noise(item)
            painter.paint_xyz(item)
            #painter.paint_3d(item)
            #painter.paint_plotly()

if (__name__ == "__main__"):
    main()
